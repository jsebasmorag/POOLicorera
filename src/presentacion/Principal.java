package presentacion;

import java.util.Scanner;

import logica.Licorera;

public class Principal {
	
	private Licorera licorera;
	
	public Principal() {
		this.licorera = new Licorera();
	}
	
	public static void main(String[] args) {
		Principal principal = new Principal();
		principal.iniciar();
	}

	private void iniciar() {
		Scanner sc = new Scanner(System.in);
		int op;
		do {
			System.out.println("Digite una opcion:\n"
					+ "0. Salir\n"
					+ "1. Ingresar Tipo Producto\n"
					+ "2. Ingresar Producto\n"
					+ "3. Ingresar Cajero\n"
					+ "4. Ingresar Cliente\n"
					+ "5. Ingresar Factura\n"
					+ "6. Imprimir Productos"
					+ "7. Imprimir Facturas");
			op = sc.nextInt();
			if(op == 1) {
				System.out.println("Digite id: ");
				int id = sc.nextInt();
				System.out.println("Digite nombre: ");
				String nombre = sc.next();				
				this.licorera.ingresarTipoProducto(id, nombre);
			}else if(op == 2) {
				System.out.println("Digite id del Tipo de Producto: ");
				int idTipoProducto = sc.nextInt();
				System.out.println("Digite codigo: ");
				int codigo = sc.nextInt();
				System.out.println("Digite nombre: ");
				String nombre = sc.next();				
				System.out.println("Digite precio compra: ");
				int precioCompra = sc.nextInt();				
				System.out.println("Digite precio venta: ");
				int precioVenta = sc.nextInt();				
				System.out.println("Digite cantidad: ");
				int cantidad = sc.nextInt();	
				this.licorera.ingresarProducto(idTipoProducto, codigo, nombre, precioCompra, precioVenta, cantidad);
			} else if(op == 6) {
				this.licorera.imprimirProductos();
			}
			
			
		}while(op != 0);
		sc.close();
	}
}
