package logica;

import java.util.ArrayList;

public class Licorera {

	private ArrayList<TipoProducto> tipoProductos;
	private ArrayList<Cajero> cajeros;
	private ArrayList<Factura> facturas;
	private ArrayList<Cliente> clientes;
	
	public Licorera() {
		this.tipoProductos = new ArrayList<TipoProducto>();
		this.cajeros = new ArrayList<Cajero>();
		this.facturas = new ArrayList<Factura>();
		this.clientes = new ArrayList<Cliente>();
		
	}

	public void ingresarTipoProducto(int id, String nombre) {
		TipoProducto nuevoTipoProducto = new TipoProducto(id, nombre);		
		this.tipoProductos.add(nuevoTipoProducto);
		
	}

	public void ingresarProducto(int idTipoProducto, int codigo, String nombre, int precioCompra, int precioVenta,
			int cantidad) {
		TipoProducto tipoProducto = this.buscarTipoProducto(idTipoProducto);
		if(tipoProducto != null) {
			tipoProducto.ingresarProducto(codigo, nombre, precioCompra, precioVenta, cantidad);
		}
		
	}

	private TipoProducto buscarTipoProducto(int idTipoProducto) {
		for(TipoProducto tipoProducto : this.tipoProductos) {
			if(tipoProducto.getId() == idTipoProducto) {
				return tipoProducto;
			}
		}
		return null;
	}

	public void imprimirProductos() {
		for(TipoProducto tipoProducto : this.tipoProductos) {
			for(Producto producto : tipoProducto.getProductos()) {
				System.out.println("Tipo: " + tipoProducto.getNombre());
				System.out.println("Codigo Producto: " + producto.getCodigo());
				System.out.println("Nombre: " + producto.getNombre());
				System.out.println("Precio Compra: " + producto.getPrecioCompra());
				System.out.println("Precio Venta: " + producto.getPrecioVenta());
				System.out.println("Cantidad: " + producto.getCantidad());
				System.out.println("-----------");
			}
		}
		
	}
}
